import { Component } from '@angular/core';
import { Plugins } from '@capacitor/core';
import { firebaseConfig } from './config/credentials';
import * as firebase from 'firebase/app';

const { SplashScreen, StatusBar } = Plugins;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor() {
    firebase.initializeApp(firebaseConfig);
    this.initializeApp();
  }

  initializeApp() {
    SplashScreen.hide().catch(err => {
      console.error(err);
    });

    StatusBar.hide().catch(err => {
      console.error(err);
    });
  }
}
